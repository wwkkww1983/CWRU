#ifndef FORM1_H
#define FORM1_H

#include <QWidget>
#include <QFileDialog> //文件选择
#include <QTextStream> //文件读写
#include <QtCharts/QChartGlobal>
#include <QtCharts/QChart>
#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include <QSplineSeries>
#include <QScatterSeries>
#include<QVector>
#include <QTableWidgetItem>

QT_CHARTS_USE_NAMESPACE
#include "ui_form1.h"


QT_BEGIN_NAMESPACE
namespace Ui {
    class form1;
}
QT_END_NAMESPACE


class form1 : public QWidget
{
    Q_OBJECT




public:
    explicit form1(QWidget *parent = 0);
    ~form1();





private:
    QChart *chart1;                      //chart对象，绘图的载体
    QChart *chart2;                      //chart对象，绘图的载体
    QChartView *chartView1;              //chartView,chart展示的载体
    QChartView *chartView2;              //chartView,chart展示的载体


    int maxX1;                           //X轴长度
    int maxX2;                           //X轴长度

    double tmax1;
    double tmax2;

    QList<double> data1;                 //存储坐标数据，使用list可以很方便的增删
    QLineSeries *splineSeries1;        //绘图数据，连续曲线
    QList<double> data2;                 //存储坐标数据，使用list可以很方便的增删
    QLineSeries *splineSeries2;        //绘图数据，连续曲线

    double feature[5];                  //时域特征参数







private slots:
    void on_readfile1_clicked();     // 读取txt1
    void on_readfile2_clicked();     // 读取txt2





private:

    Ui::form1 *ui;

    void CreatChart();
    QChart *buildChart1();
    QChart *buildChart2();
    void update1();
    void update2();

};


#endif // FORM1_H
