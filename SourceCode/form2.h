#ifndef FORM2_H
#define FORM2_H

#include <QWidget>
#include <QWidget>
#include <QFileDialog> //文件选择
#include <QTextStream> //文件读写
#include <QtCharts/QChartGlobal>
#include <QtCharts/QChart>
#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include <QSplineSeries>
#include <QScatterSeries>
#include<QVector>
#include <QTableWidgetItem>


/*FFTW*/
#include <fftw3.h>
#include <stdlib.h>


namespace Ui {
    class form2;
}

class form2 : public QWidget
{
    Q_OBJECT


private:

    QChart *fchart1;                      //chart对象，绘图的载体
    QChart *fchart2;                      //chart对象，绘图的载体
    QChartView *fchartView1;              //chartView,chart展示的载体
    QChartView *fchartView2;
    QLineSeries *fsplineSeries1;        //绘图数据，连续曲线
    QLineSeries *fsplineSeries2;        //绘图数据，连续曲线

    double fmax1;
    double fmax2;
    std::vector<std::vector<double>> vmax1;
    QScatterSeries *fscatterSeries1;


    int maxX;                           //X轴长度
    float maxY;                           //y轴最大值
    float minY;                           //y轴最小值

    int N = 1024;



    QList<double> fdata1;               //数据1
    QList<double> fdata2;               //数据2
    QString fdataname1;                 //数据1文件路径
    QString fdataname2;                 //数据2文件路径

    QList<double> FFTans1;              //数据1变换后的幅值
    QList<double> FFTans2;              //数据2变换后的幅值

public:
    explicit form2(QWidget *parent = 0);
    ~form2();
    void slotPointHoverd(const QPointF &point, bool state);



private slots:
    void on_cvrd1_clicked();        //读取频域分析文件1
    void on_cvrd2_clicked();        //读取频域分析文件2
    void on_redraw_clicked();       //重新绘图

private:
    void FFTW1();
    void FFTW2();

    void fCreatChart();
    QChart *fbuildChart1();
    QChart *fbuildChart2();
    void fupdate1();
    void fupdate2();

private:
    Ui::form2 *ui;
};


#endif // FORM1_H
