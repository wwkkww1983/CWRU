#ifndef FORM3_H
#define FORM3_H

#include <QFileDialog> //文件选择
#include <QTextStream> //文件读写
#include <QtCharts/QChartGlobal>
#include <QtCharts/QChart>
#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include <QSplineSeries>
#include <QScatterSeries>
#include<QVector>
#include <QTableWidgetItem>

namespace Ui {
    class form3;
}

class form3 : public QWidget
{
    Q_OBJECT

public:
    explicit form3(QWidget *parent = 0);
    ~form3();


private slots:
    void on_pushButton_clicked();

private:
    Ui::form3 *ui;
};


#endif // FORM1_H
