#include <QDebug>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "form1.h"
#include "form2.h"
#include "form3.h"
#include "form4.h"


MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow){

    ui->setupUi(this);

    ui->form1->setDefault(true);

    form1Widget = new form1(this);
    form2Widget = new form2(this);
    form3Widget = new form3(this);
    form4Widget = new form4(this);

    ui->stackedWidget->addWidget(form1Widget);
    ui->stackedWidget->addWidget(form2Widget);
    ui->stackedWidget->addWidget(form3Widget);
    ui->stackedWidget->addWidget(form4Widget);
    ui->stackedWidget->setCurrentWidget(form1Widget);


}

MainWindow::~MainWindow()
{
    delete ui;
    delete form1Widget;
    delete form2Widget;
    delete form3Widget;
    delete form4Widget;
}


void MainWindow::on_form1_clicked(){
    if(ui->stackedWidget->currentWidget() != form1Widget){
            ui->stackedWidget->setCurrentWidget(form1Widget);
            ui->form2->setDefault(false);
            ui->form1->setDefault(true);
            ui->form3->setDefault(false);
            ui->form4->setDefault(false);
        }
}


void MainWindow::on_form2_clicked(){
    if(ui->stackedWidget->currentWidget() != form2Widget){
            ui->stackedWidget->setCurrentWidget(form2Widget);
            ui->form1->setDefault(false);
            ui->form2->setDefault(true);
            ui->form3->setDefault(false);
            ui->form4->setDefault(false);
    }
}



void MainWindow::on_form3_clicked(){
    if(ui->stackedWidget->currentWidget() != form3Widget){
            ui->stackedWidget->setCurrentWidget(form3Widget);
            ui->form1->setDefault(false);
            ui->form3->setDefault(true);
            ui->form2->setDefault(false);
            ui->form4->setDefault(false);
    }
}

void MainWindow::on_form4_clicked(){
    if(ui->stackedWidget->currentWidget() != form4Widget){
        ui->stackedWidget->setCurrentWidget(form4Widget);
        ui->form1->setDefault(false);
        ui->form2->setDefault(false);
        ui->form3->setDefault(false);
        ui->form4->setDefault(true);
    }
}

