


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QListWidget>//列表框
#include <QStackedWidget>//堆栈窗体
#include <QLabel>

#include <QMainWindow>
#include <QEvent>
#include <QString>
#include <QMouseEvent>
#include "form1.h"
#include "form2.h"
#include "form3.h"
#include "form4.h"




namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:

    Ui::MainWindow *ui;
    form1 *form1Widget;
    form2 *form2Widget;
    form3 *form3Widget;
    form4 *form4Widget;


private slots:
    void on_form1_clicked();
    void on_form2_clicked();
    void on_form3_clicked();
    void on_form4_clicked();

};

#endif // STACKWINDOW_H
