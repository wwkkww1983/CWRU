/****************************************************************************
** Meta object code from reading C++ file 'form4.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../SourceCode/form4.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'form4.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_form4_t {
    QByteArrayData data[23];
    char stringdata0[466];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_form4_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_form4_t qt_meta_stringdata_form4 = {
    {
QT_MOC_LITERAL(0, 0, 5), // "form4"
QT_MOC_LITERAL(1, 6, 22), // "on_txt_button1_clicked"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 17), // "on_clear1_clicked"
QT_MOC_LITERAL(4, 48, 24), // "on_label_button1_clicked"
QT_MOC_LITERAL(5, 73, 22), // "on_txt_button2_clicked"
QT_MOC_LITERAL(6, 96, 17), // "on_clear2_clicked"
QT_MOC_LITERAL(7, 114, 24), // "on_label_button2_clicked"
QT_MOC_LITERAL(8, 139, 22), // "on_txt_button3_clicked"
QT_MOC_LITERAL(9, 162, 17), // "on_clear3_clicked"
QT_MOC_LITERAL(10, 180, 24), // "on_label_button3_clicked"
QT_MOC_LITERAL(11, 205, 22), // "on_txt_button4_clicked"
QT_MOC_LITERAL(12, 228, 17), // "on_clear4_clicked"
QT_MOC_LITERAL(13, 246, 24), // "on_label_button4_clicked"
QT_MOC_LITERAL(14, 271, 20), // "on_savelabel_clicked"
QT_MOC_LITERAL(15, 292, 20), // "on_readlabel_clicked"
QT_MOC_LITERAL(16, 313, 23), // "on_verification_clicked"
QT_MOC_LITERAL(17, 337, 20), // "on_saveModel_clicked"
QT_MOC_LITERAL(18, 358, 21), // "on_trainBPnet_clicked"
QT_MOC_LITERAL(19, 380, 20), // "on_loadmodel_clicked"
QT_MOC_LITERAL(20, 401, 19), // "on_analysis_clicked"
QT_MOC_LITERAL(21, 421, 22), // "on_readansdata_clicked"
QT_MOC_LITERAL(22, 444, 21) // "on_trainitems_clicked"

    },
    "form4\0on_txt_button1_clicked\0\0"
    "on_clear1_clicked\0on_label_button1_clicked\0"
    "on_txt_button2_clicked\0on_clear2_clicked\0"
    "on_label_button2_clicked\0"
    "on_txt_button3_clicked\0on_clear3_clicked\0"
    "on_label_button3_clicked\0"
    "on_txt_button4_clicked\0on_clear4_clicked\0"
    "on_label_button4_clicked\0on_savelabel_clicked\0"
    "on_readlabel_clicked\0on_verification_clicked\0"
    "on_saveModel_clicked\0on_trainBPnet_clicked\0"
    "on_loadmodel_clicked\0on_analysis_clicked\0"
    "on_readansdata_clicked\0on_trainitems_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_form4[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x08 /* Private */,
       3,    0,  120,    2, 0x08 /* Private */,
       4,    0,  121,    2, 0x08 /* Private */,
       5,    0,  122,    2, 0x08 /* Private */,
       6,    0,  123,    2, 0x08 /* Private */,
       7,    0,  124,    2, 0x08 /* Private */,
       8,    0,  125,    2, 0x08 /* Private */,
       9,    0,  126,    2, 0x08 /* Private */,
      10,    0,  127,    2, 0x08 /* Private */,
      11,    0,  128,    2, 0x08 /* Private */,
      12,    0,  129,    2, 0x08 /* Private */,
      13,    0,  130,    2, 0x08 /* Private */,
      14,    0,  131,    2, 0x08 /* Private */,
      15,    0,  132,    2, 0x08 /* Private */,
      16,    0,  133,    2, 0x08 /* Private */,
      17,    0,  134,    2, 0x08 /* Private */,
      18,    0,  135,    2, 0x08 /* Private */,
      19,    0,  136,    2, 0x08 /* Private */,
      20,    0,  137,    2, 0x08 /* Private */,
      21,    0,  138,    2, 0x08 /* Private */,
      22,    0,  139,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void form4::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<form4 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_txt_button1_clicked(); break;
        case 1: _t->on_clear1_clicked(); break;
        case 2: _t->on_label_button1_clicked(); break;
        case 3: _t->on_txt_button2_clicked(); break;
        case 4: _t->on_clear2_clicked(); break;
        case 5: _t->on_label_button2_clicked(); break;
        case 6: _t->on_txt_button3_clicked(); break;
        case 7: _t->on_clear3_clicked(); break;
        case 8: _t->on_label_button3_clicked(); break;
        case 9: _t->on_txt_button4_clicked(); break;
        case 10: _t->on_clear4_clicked(); break;
        case 11: _t->on_label_button4_clicked(); break;
        case 12: _t->on_savelabel_clicked(); break;
        case 13: _t->on_readlabel_clicked(); break;
        case 14: _t->on_verification_clicked(); break;
        case 15: _t->on_saveModel_clicked(); break;
        case 16: _t->on_trainBPnet_clicked(); break;
        case 17: _t->on_loadmodel_clicked(); break;
        case 18: _t->on_analysis_clicked(); break;
        case 19: _t->on_readansdata_clicked(); break;
        case 20: _t->on_trainitems_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject form4::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_form4.data,
    qt_meta_data_form4,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *form4::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *form4::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_form4.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int form4::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
