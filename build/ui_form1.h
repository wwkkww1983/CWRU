/********************************************************************************
** Form generated from reading UI file 'form1.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM1_H
#define UI_FORM1_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qchartview.h"

QT_BEGIN_NAMESPACE

class Ui_form1
{
public:
    QChartView *chartView1;
    QPushButton *readfile1;
    QPushButton *readfile2;
    QChartView *chartView2;
    QLabel *label_13;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QTextBrowser *f1qiaodu1;
    QTextBrowser *f1fengzhi1;
    QTextBrowser *f1yudu1;
    QTextBrowser *f1maichong1;
    QTextBrowser *f1boxing1;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_5;
    QTextBrowser *f1qiaodu2;
    QTextBrowser *f1fengzhi2;
    QTextBrowser *f1yudu2;
    QTextBrowser *f1maichong2;
    QTextBrowser *f1boxing2;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;

    void setupUi(QWidget *form1)
    {
        if (form1->objectName().isEmpty())
            form1->setObjectName(QString::fromUtf8("form1"));
        form1->resize(1000, 600);
        chartView1 = new QChartView(form1);
        chartView1->setObjectName(QString::fromUtf8("chartView1"));
        chartView1->setGeometry(QRect(0, 40, 781, 241));
        readfile1 = new QPushButton(form1);
        readfile1->setObjectName(QString::fromUtf8("readfile1"));
        readfile1->setGeometry(QRect(0, 10, 141, 28));
        readfile2 = new QPushButton(form1);
        readfile2->setObjectName(QString::fromUtf8("readfile2"));
        readfile2->setGeometry(QRect(0, 300, 131, 28));
        chartView2 = new QChartView(form1);
        chartView2->setObjectName(QString::fromUtf8("chartView2"));
        chartView2->setGeometry(QRect(0, 330, 781, 231));
        label_13 = new QLabel(form1);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(790, 30, 191, 251));
        label_13->setFrameShape(QFrame::Box);
        verticalLayoutWidget_4 = new QWidget(form1);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(800, 40, 102, 231));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(verticalLayoutWidget_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        QFont font;
        font.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font.setPointSize(12);
        label_6->setFont(font);

        verticalLayout_4->addWidget(label_6);

        label_7 = new QLabel(verticalLayoutWidget_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        verticalLayout_4->addWidget(label_7);

        label_8 = new QLabel(verticalLayoutWidget_4);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);

        verticalLayout_4->addWidget(label_8);

        label_9 = new QLabel(verticalLayoutWidget_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font);

        verticalLayout_4->addWidget(label_9);

        label_10 = new QLabel(verticalLayoutWidget_4);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);

        verticalLayout_4->addWidget(label_10);

        verticalLayoutWidget_3 = new QWidget(form1);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(900, 40, 71, 231));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        f1qiaodu1 = new QTextBrowser(verticalLayoutWidget_3);
        f1qiaodu1->setObjectName(QString::fromUtf8("f1qiaodu1"));
        f1qiaodu1->setEnabled(false);

        verticalLayout_3->addWidget(f1qiaodu1);

        f1fengzhi1 = new QTextBrowser(verticalLayoutWidget_3);
        f1fengzhi1->setObjectName(QString::fromUtf8("f1fengzhi1"));
        f1fengzhi1->setEnabled(false);

        verticalLayout_3->addWidget(f1fengzhi1);

        f1yudu1 = new QTextBrowser(verticalLayoutWidget_3);
        f1yudu1->setObjectName(QString::fromUtf8("f1yudu1"));
        f1yudu1->setEnabled(false);

        verticalLayout_3->addWidget(f1yudu1);

        f1maichong1 = new QTextBrowser(verticalLayoutWidget_3);
        f1maichong1->setObjectName(QString::fromUtf8("f1maichong1"));
        f1maichong1->setEnabled(false);

        verticalLayout_3->addWidget(f1maichong1);

        f1boxing1 = new QTextBrowser(verticalLayoutWidget_3);
        f1boxing1->setObjectName(QString::fromUtf8("f1boxing1"));
        f1boxing1->setEnabled(false);

        verticalLayout_3->addWidget(f1boxing1);

        verticalLayoutWidget_5 = new QWidget(form1);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(900, 340, 71, 231));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        f1qiaodu2 = new QTextBrowser(verticalLayoutWidget_5);
        f1qiaodu2->setObjectName(QString::fromUtf8("f1qiaodu2"));
        f1qiaodu2->setEnabled(false);

        verticalLayout_5->addWidget(f1qiaodu2);

        f1fengzhi2 = new QTextBrowser(verticalLayoutWidget_5);
        f1fengzhi2->setObjectName(QString::fromUtf8("f1fengzhi2"));
        f1fengzhi2->setEnabled(false);

        verticalLayout_5->addWidget(f1fengzhi2);

        f1yudu2 = new QTextBrowser(verticalLayoutWidget_5);
        f1yudu2->setObjectName(QString::fromUtf8("f1yudu2"));
        f1yudu2->setEnabled(false);

        verticalLayout_5->addWidget(f1yudu2);

        f1maichong2 = new QTextBrowser(verticalLayoutWidget_5);
        f1maichong2->setObjectName(QString::fromUtf8("f1maichong2"));
        f1maichong2->setEnabled(false);

        verticalLayout_5->addWidget(f1maichong2);

        f1boxing2 = new QTextBrowser(verticalLayoutWidget_5);
        f1boxing2->setObjectName(QString::fromUtf8("f1boxing2"));
        f1boxing2->setEnabled(false);

        verticalLayout_5->addWidget(f1boxing2);

        verticalLayoutWidget_6 = new QWidget(form1);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(800, 340, 102, 231));
        verticalLayout_6 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        label_11 = new QLabel(verticalLayoutWidget_6);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font);

        verticalLayout_6->addWidget(label_11);

        label_12 = new QLabel(verticalLayoutWidget_6);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setFont(font);

        verticalLayout_6->addWidget(label_12);

        label_14 = new QLabel(verticalLayoutWidget_6);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font);

        verticalLayout_6->addWidget(label_14);

        label_15 = new QLabel(verticalLayoutWidget_6);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font);

        verticalLayout_6->addWidget(label_15);

        label_16 = new QLabel(verticalLayoutWidget_6);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font);

        verticalLayout_6->addWidget(label_16);

        label_17 = new QLabel(form1);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(790, 330, 191, 251));
        label_17->setFrameShape(QFrame::Box);
        label_17->raise();
        chartView1->raise();
        readfile1->raise();
        readfile2->raise();
        chartView2->raise();
        label_13->raise();
        verticalLayoutWidget_4->raise();
        verticalLayoutWidget_3->raise();
        verticalLayoutWidget_5->raise();
        verticalLayoutWidget_6->raise();

        retranslateUi(form1);

        QMetaObject::connectSlotsByName(form1);
    } // setupUi

    void retranslateUi(QWidget *form1)
    {
        form1->setWindowTitle(QCoreApplication::translate("form1", "Form", nullptr));
        readfile1->setText(QCoreApplication::translate("form1", "\350\257\273\345\217\226\346\226\207\344\273\266", nullptr));
        readfile2->setText(QCoreApplication::translate("form1", "\350\257\273\345\217\226\346\226\207\344\273\266", nullptr));
        label_13->setText(QString());
        label_6->setText(QCoreApplication::translate("form1", "\345\263\255\345\272\246\346\214\207\346\240\207\357\274\232", nullptr));
        label_7->setText(QCoreApplication::translate("form1", "\345\263\260\345\200\274\346\214\207\346\240\207\357\274\232", nullptr));
        label_8->setText(QCoreApplication::translate("form1", "\350\243\225\345\272\246\346\214\207\346\240\207\357\274\232", nullptr));
        label_9->setText(QCoreApplication::translate("form1", "\350\204\211\345\206\262\346\214\207\346\240\207\357\274\232", nullptr));
        label_10->setText(QCoreApplication::translate("form1", "\346\263\242\345\275\242\346\214\207\346\240\207\357\274\232", nullptr));
        label_11->setText(QCoreApplication::translate("form1", "\345\263\255\345\272\246\346\214\207\346\240\207\357\274\232", nullptr));
        label_12->setText(QCoreApplication::translate("form1", "\345\263\260\345\200\274\346\214\207\346\240\207\357\274\232", nullptr));
        label_14->setText(QCoreApplication::translate("form1", "\350\243\225\345\272\246\346\214\207\346\240\207\357\274\232", nullptr));
        label_15->setText(QCoreApplication::translate("form1", "\350\204\211\345\206\262\346\214\207\346\240\207\357\274\232", nullptr));
        label_16->setText(QCoreApplication::translate("form1", "\346\263\242\345\275\242\346\214\207\346\240\207\357\274\232", nullptr));
        label_17->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class form1: public Ui_form1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM1_H
