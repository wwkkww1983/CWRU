/********************************************************************************
** Form generated from reading UI file 'form2.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM2_H
#define UI_FORM2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "qchartview.h"

QT_BEGIN_NAMESPACE

class Ui_form2
{
public:
    QChartView *fchartView1;
    QPushButton *cvrd1;
    QPushButton *cvrd2;
    QLabel *m_valueLabel;
    QPushButton *redraw;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *comboBox;
    QChartView *fchartView2;

    void setupUi(QWidget *form2)
    {
        if (form2->objectName().isEmpty())
            form2->setObjectName(QString::fromUtf8("form2"));
        form2->resize(1000, 600);
        fchartView1 = new QChartView(form2);
        fchartView1->setObjectName(QString::fromUtf8("fchartView1"));
        fchartView1->setGeometry(QRect(10, 30, 791, 251));
        cvrd1 = new QPushButton(form2);
        cvrd1->setObjectName(QString::fromUtf8("cvrd1"));
        cvrd1->setGeometry(QRect(10, 0, 93, 28));
        cvrd2 = new QPushButton(form2);
        cvrd2->setObjectName(QString::fromUtf8("cvrd2"));
        cvrd2->setGeometry(QRect(10, 310, 93, 28));
        m_valueLabel = new QLabel(form2);
        m_valueLabel->setObjectName(QString::fromUtf8("m_valueLabel"));
        m_valueLabel->setGeometry(QRect(870, 350, 91, 31));
        m_valueLabel->setFrameShape(QFrame::NoFrame);
        redraw = new QPushButton(form2);
        redraw->setObjectName(QString::fromUtf8("redraw"));
        redraw->setGeometry(QRect(820, 60, 91, 41));
        QFont font;
        font.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font.setPointSize(11);
        redraw->setFont(font);
        horizontalLayoutWidget = new QWidget(form2);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(820, 110, 131, 51));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font1.setPointSize(14);
        label->setFont(font1);

        horizontalLayout->addWidget(label);

        comboBox = new QComboBox(horizontalLayoutWidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font2.setPointSize(12);
        comboBox->setFont(font2);

        horizontalLayout->addWidget(comboBox);

        fchartView2 = new QChartView(form2);
        fchartView2->setObjectName(QString::fromUtf8("fchartView2"));
        fchartView2->setGeometry(QRect(10, 340, 791, 251));

        retranslateUi(form2);

        QMetaObject::connectSlotsByName(form2);
    } // setupUi

    void retranslateUi(QWidget *form2)
    {
        form2->setWindowTitle(QCoreApplication::translate("form2", "Form", nullptr));
        cvrd1->setText(QCoreApplication::translate("form2", "\350\257\273\345\217\226\346\226\207\344\273\266", nullptr));
        cvrd2->setText(QCoreApplication::translate("form2", "\350\257\273\345\217\226\346\226\207\344\273\266", nullptr));
        m_valueLabel->setText(QCoreApplication::translate("form2", "haha ", nullptr));
        redraw->setText(QCoreApplication::translate("form2", "\351\207\215\346\226\260\347\273\230\345\233\276", nullptr));
        label->setText(QCoreApplication::translate("form2", "N\357\274\232", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("form2", "1024", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("form2", "2048", nullptr));
        comboBox->setItemText(2, QCoreApplication::translate("form2", "4096", nullptr));
        comboBox->setItemText(3, QCoreApplication::translate("form2", "6144", nullptr));
        comboBox->setItemText(4, QCoreApplication::translate("form2", "8192", nullptr));
        comboBox->setItemText(5, QCoreApplication::translate("form2", "10240", nullptr));

    } // retranslateUi

};

namespace Ui {
    class form2: public Ui_form2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM2_H
