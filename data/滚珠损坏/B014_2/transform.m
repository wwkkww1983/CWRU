clc;clear;close all;
load 187.mat

workbook1 = 'data187_BA.txt';
data1 = X187_BA_time;

workbook2 = 'data187_DE.txt';
data2 = X187_DE_time;

workbook3 = 'data187_FE.txt';
data3 = X187_FE_time;

workbook4 = 'data187_RPM.txt';
data4 = X187RPM;


txt1 = fopen(workbook1,'w');
for i = 1:length(data1)-1
    fprintf(txt1,'%.15f\n',data1(i));
end
fprintf(txt1,'%.15f',data1(length(data1)));



txt2 = fopen(workbook2,'w');
for i = 1:length(data2)-1
    fprintf(txt2,'%.15f\n',data2(i));
end
fprintf(txt2,'%.15f',data2(length(data2)));




txt3 = fopen(workbook3,'w');
for i = 1:length(data3) - 1
    fprintf(txt3,'%.15f\n',data3(i));
end
fprintf(txt3,'%.15f',data3(length(data3)));


txt4 = fopen(workbook4,'w');
for i = 1:length(data4)
    fprintf(txt4,'%d',data4(i));
end

